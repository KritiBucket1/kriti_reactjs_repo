import React from 'react';
import logo from './logo.svg';
import './App.css';
import RandomAsteroid from './components/RandomAsteroid'
import InputForm from './components/InputForm'
function App() {
  return (
    <div className="App">      
      <InputForm/>
      <RandomAsteroid/>
    </div>    
  );
}

export default App;
