import React, { Component } from 'react'
import axios from 'axios'

class InputForm extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             asteroidId: ''
        }
    }
    handleAsteriodIdChange = (event) =>{
        this.setState({
            asteroidId: event.target.value
        })
    }
    handleFormSubmit = (event) =>{
        event.preventDefault()
        axios.get(`https://api.nasa.gov/neo/rest/v1/neo/${this.state.asteroidId}?api_key=5TbCeEhZZTc1dbI1COHK6pWzhU7Enu0m7y0sAAyQ`)
        .then(response => {
            console.log(response)     
            this.setState({asteroidIds: response.data})       
        })
        .catch(error =>{
            console.log(error)
        })
    }
    render() {
        const { asteroidIds } = this.state

        return (
            <form onSubmit={this.handleFormSubmit}>
                <div>
                <label>Asteroid Id</label>:
                <input type="text" value={this.state.asteroidId} onChange={this.handleAsteriodIdChange}/>                
                </div>
                <button type="submit" disabled={!this.state.asteroidId}>Submit</button>  
            </form>            
        )
    }
}

export default InputForm
